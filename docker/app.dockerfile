FROM maven:3.6-jdk-8-alpine AS build-env
VOLUME "$HOME/.m2" /root/.m2
WORKDIR /app
ADD . /app
RUN cd /app 
RUN mvn clean install

FROM openjdk:8-jre-alpine
RUN apk update && \
   apk add ca-certificates && \
   update-ca-certificates && \
   rm -rf /var/cache/apk/*
   
WORKDIR /app
COPY --from=build-env /app/target/*.jar /app/produto-api.jar

EXPOSE 8080

CMD [ "sh", "-c", "java $JAVA_OPTS -Dfile.encoding=UTF-8 -Djava.security.egd=file:/dev/./urandom -jar /app/produto-api.jar" ]
