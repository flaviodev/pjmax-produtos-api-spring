#!/bin/bash

cd ../../

docker rm node-dev -f

docker run -it --name node-dev --net host -v "$(pwd)"/src/frontend:/usr/src:rw -w /usr/src/produto-ui node-dev sh
