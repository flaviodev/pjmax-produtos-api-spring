#!/bin/bash

cd ../../

app_path=`pwd`

docker rm node-dev -f
docker run -it --name node-dev --net host -v $app_path/src/frontend:/usr/src:rw -w /usr/src/produto-ui node-dev ng build

cp src/frontend/dist/* src/main/resources/static/
