FROM postgres:9.6-alpine

MAINTAINER Flavio de Souza <fdsdev@gmail.com>

ENV LANG pt_BR.UTF-8
ENV POSTGRES_PASSWORD maxima

ADD files-postgres/init-db.sh /docker-entrypoint-initdb.d

EXPOSE 5432
