# Cadastro de Produtos

Para acessar a aplicação: http://localhost:8080

Existem várias formas de executar o projeto:

* Docker-compose (baixando as imagens do docker hub):

```
$ <pasta-do-projeto>/docker/docker-compose up
```

* Docker-compose (efetuando o build local das imagens):

```
$ <pasta-do-projeto>/docker/docker-compose up -f docker-compose-build-images.yml
```

* Executar pelo Spring Boot apontando para banco local instalado (o PostgreSql deve ter o banco bdmaxima, se já houver o banco é recomendável apagar e criar um novo):

```
$ <pasta-do-projeto>/mvn spring-boot:run -Dspring-boot.run.profiles=local 
```

* Executar pelo Spring Boot apontando para o banco do docker:

```
$ <pasta-do-projeto>/docker/docker-compose -d up db

$ <pasta-do-projeto>/mvn spring-boot:run -Dspring-boot.run.profiles=local 
 
```

