import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { ProdutoService } from './produto.service';


@Injectable()
export class GetProdutosResolver implements Resolve<any> {
  constructor(private produtoService: ProdutoService) {}

  resolve() {
    console.log("Resolving ..... get produtos");
    return this.produtoService.getProdutos();
  }
}
