import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource, MatDialog, MatSnackBar} from '@angular/material';

import { ProdutoService } from '../produto.service';
import { Router } from '@angular/router';
import { DialogConfirmaExclusao } from './dialog-confirma-exclusao.component';

@Component({
  selector: 'app-listar-produto',
  templateUrl: './listar-produto.component.html',
  styleUrls: ['./listar-produto.component.css']
})
export class ListarProdutoComponent implements OnInit {
  
  displayedColumns: string[] = ['descricao', 'preco', 'dataHoraCadastro', 'btVisualizar', 'btAlterar', 'btExcluir'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatSort) sort: MatSort;

  constructor(private servicoProduto: ProdutoService, private router: Router, 
    public dialog: MatDialog, public snackBar: MatSnackBar) {
 
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.carregaProdutos();
  }

  carregaProdutos() {
    this.servicoProduto.getProdutos().subscribe(data => this.dataSource.data = data);
  }

  confirmaExclusao(produto) {
    const dialogRef = this.dialog.open(DialogConfirmaExclusao, {
      width: '350px',
      data: {produto: produto}
    });

    dialogRef.afterClosed().subscribe(produto => {
      console.log(produto);

      if(produto) {
        this.servicoProduto.excluirProduto(produto.id)
            .subscribe(response =>  {
              this.mostrarMensagemDeSucesso('Produto excluído com sucesso!');
              this.dataSource.data = this.dataSource.data.filter((p:any) => p.id != produto.id);
            });        
      }          
    });
    
  }

  mostrarMensagemDeSucesso(mensagem: string) {
      this.snackBar.open(mensagem, '', {
              duration: 3000
      });
  }
}
