import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule} from '@angular/material/table';
import { MatSortModule, MatIconModule, MatButtonModule, MatTooltipModule, MatDialogModule, MatSnackBarModule } from '@angular/material';
import { MatCardModule} from '@angular/material/card';
import { Routes, RouterModule } from '@angular/router';
import { GetProdutosResolver } from '../produto.resolve';
import { ListarProdutoComponent } from './listar-produto.component';
import { DialogConfirmaExclusao } from './dialog-confirma-exclusao.component';

const routes: Routes = [
  {path: '', component: ListarProdutoComponent, 
  resolve: { items: GetProdutosResolver }}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatSortModule,
    MatTableModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule, 
    MatDialogModule,
    MatSnackBarModule
  ],
  declarations: [
    ListarProdutoComponent,
    DialogConfirmaExclusao
  ],
  entryComponents : [DialogConfirmaExclusao],
  exports : [RouterModule]
})
export class ListarProdutoModule { 
}
