import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

@Component({
    selector: 'dialog-confirma-exclusao',
    templateUrl: 'dialog-confirma-exclusao.component.html',
})
export class DialogConfirmaExclusao {
  
    constructor(public dialogRef: MatDialogRef<DialogConfirmaExclusao>, 
        @Inject(MAT_DIALOG_DATA) public data) {}
  
    onCancelaClick() {
        this.dialogRef.close();
    }   
}
