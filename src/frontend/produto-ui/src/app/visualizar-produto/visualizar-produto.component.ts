import { Component, OnInit } from '@angular/core';
import { ProdutoService } from '../produto.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-visualizar-produto',
  templateUrl: './visualizar-produto.component.html',
  styleUrls: ['./visualizar-produto.component.css']
})
export class VisualizarProdutoComponent implements OnInit {

  titulo = "";
  produto = {};

  constructor(private servicoProduto: ProdutoService, private route: ActivatedRoute,
    private router: Router) {
  }
 
  ngOnInit() {
    this.route.params.subscribe(params => {
      if(params['id']){
        this.titulo = "Dados do Produto";
        this.servicoProduto.getProdutoPeloId(params['id'])
          .subscribe(_produto => this.produto = _produto);
      }else{
        this.titulo = "Dados do Último Produto Cadastrado"
        this.servicoProduto.getUltimoProdutoCadastrado()
          .subscribe(_produto => this.produto = _produto);
      }});
  }
}
