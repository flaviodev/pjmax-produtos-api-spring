import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisualizarProdutoComponent } from './visualizar-produto.component';
import { RouterModule, Routes } from '@angular/router';
import { MatCardModule, MatButtonModule } from '@angular/material';

const routes: Routes = [
  {path: '', component: VisualizarProdutoComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    MatButtonModule
  ],
  declarations: [VisualizarProdutoComponent],
  exports : [RouterModule]
})
export class VisualizarProdutoModule { }
