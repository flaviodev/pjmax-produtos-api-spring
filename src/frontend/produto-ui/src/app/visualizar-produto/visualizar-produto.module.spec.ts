import { VisualizarProdutoModule } from './visualizar-produto.module';

describe('VisualizarProdutoModule', () => {
  let visualizarProdutoModule: VisualizarProdutoModule;

  beforeEach(() => {
    visualizarProdutoModule = new VisualizarProdutoModule();
  });

  it('should create an instance', () => {
    expect(visualizarProdutoModule).toBeTruthy();
  });
});
