import { Routes, RouterModule } from '@angular/router';

const ROUTES: Routes = [
    { path: 'listarproduto', loadChildren: './listar-produto/listar-produto.module#ListarProdutoModule'},
    { path: '',redirectTo: 'listarproduto',pathMatch: 'full'},
    { path: 'cadastrarproduto', loadChildren: './cadastrar-produto/cadastrar-produto.module#CadastrarProdutoModule'},
    { path: 'alterarproduto/:id', loadChildren: './cadastrar-produto/cadastrar-produto.module#CadastrarProdutoModule'},
    { path: 'ultimoproduto', loadChildren: './visualizar-produto/visualizar-produto.module#VisualizarProdutoModule'},
    { path: 'visualizarproduto/:id', loadChildren: './visualizar-produto/visualizar-produto.module#VisualizarProdutoModule'}
];

export const routing = RouterModule.forRoot(ROUTES, { useHash: true }); //, enableTracing: true });