import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule, MatIconModule, MatCardModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatSnackBarModule } from '@angular/material';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CadastrarProdutoComponent } from './cadastrar-produto.component';

const routes: Routes = [
  {path: '', component: CadastrarProdutoComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule, 
    MatFormFieldModule,
    CurrencyMaskModule,
    MatInputModule,
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [CadastrarProdutoComponent],
  exports : [RouterModule]
})
export class CadastrarProdutoModule { }
