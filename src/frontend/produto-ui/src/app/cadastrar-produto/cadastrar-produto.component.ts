import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ProdutoService } from '../produto.service';

@Component({
  selector: 'app-cadastrar-produto',
  templateUrl: './cadastrar-produto.component.html',
  styleUrls: ['./cadastrar-produto.component.css']
})
export class CadastrarProdutoComponent implements OnInit {

  titulo = ""
  isInclusao = true;
  produto:any = {};

  descricao = new FormControl('', [Validators.required]);
  preco = new FormControl('', [Validators.required, Validators.min(0.01)]);

  formGrupoProduto = this.formBuilder.group({
    descricao: this.descricao,
    preco: this.preco
  })

  getMessageErroDesricao() {
    return this.descricao.hasError('required') ? 'A descrição do produto é obrigatória' : '';
  }
  
  getMessageErroPreco() {
    return this.preco.hasError('required') ? 'Informe o preço do produto' : 
      this.preco.hasError('min') ? 'O preço deve ser maior que zero' : '';
  }

  constructor(private servicoProduto: ProdutoService, private route: ActivatedRoute,
                private router: Router, private formBuilder: FormBuilder, public snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if(params['id']){
        this.titulo = "Alterar Produto";
        this.servicoProduto.getProdutoPeloId(params['id'])
          .subscribe(_produto => this.produto = _produto);
        this.isInclusao = false;
      }else{
        this.titulo = "Cadastrar Produto";
        this.isInclusao = true;
      }});
  }

  salvar() {
    if(this.isInclusao) {
      this.servicoProduto.incluirProduto(this.produto)
        .subscribe(response => this.mostrarMensagemDeSucesso('Produto cadastrado com sucesso!'));
    } else {
      console.log(this.produto.preco);
      this.servicoProduto.alterarProduto(this.produto)
        .subscribe(response => this.mostrarMensagemDeSucesso('Produto alterado com sucesso!'));
    }
  }
  
  mostrarMensagemDeSucesso(mensagem: string) {
    this.snackBar.open(mensagem, '', {
      duration: 3000
    });
    this.router.navigate(['/listarproduto']); 
  }
}