import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { routing } from './app.routing';

import { CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { MascaraValorMonetarioBrasil } from './app.config';
import { ProdutoService } from './produto.service';
import { MenuModule } from './menu/menu.module';
import { CadastrarProdutoModule } from './cadastrar-produto/cadastrar-produto.module';
import { VisualizarProdutoModule } from './visualizar-produto/visualizar-produto.module';
import { GetProdutosResolver } from './produto.resolve';

registerLocaleData(localePt, 'pt-BR');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    routing,
    MenuModule,
    CadastrarProdutoModule,
    VisualizarProdutoModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: "pt-BR"
    },
    { 
      provide: CURRENCY_MASK_CONFIG, 
      useValue: MascaraValorMonetarioBrasil 
    },
    ProdutoService, 
    GetProdutosResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
