import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { GetProdutosResolver } from './produto.resolve';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  
  constructor(private http: HttpClient) { }

  getProdutos() {
    return this.http.get<Produto[]>("/produto");
  }

  getProdutoPeloId(idDoProduto) {
    return this.http.get<Produto>("/produto/" + idDoProduto);
  }

  getUltimoProdutoCadastrado() {
    return this.http.get<Produto>("/produto/ultimoCadastrado");
  }

  incluirProduto(produto) {
    return this.http.post('/produto', produto);
  }

  alterarProduto(produto) {
    return this.http.put('/produto/' + produto.id, produto);
  }

  excluirProduto(idDoProduto) {
    return this.http.delete("/produto/" + idDoProduto);
  }
}
