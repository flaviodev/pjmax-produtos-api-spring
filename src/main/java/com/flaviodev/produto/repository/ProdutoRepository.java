package com.flaviodev.produto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flaviodev.produto.model.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, String> {
	List<Produto> findAllByDescricaoStartsWithOrderByDescricao(String descricao);
	
	List<Produto> findAllByOrderByDescricao();
	
	Produto findFirstByOrderByDataHoraCadastroDesc();
}