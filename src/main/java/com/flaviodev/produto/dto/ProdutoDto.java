package com.flaviodev.produto.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.flaviodev.produto.dto.base.Dto;
import com.flaviodev.produto.model.Produto;

public class ProdutoDto extends Dto<String> {

	private static final long serialVersionUID = 4751066819227624636L;

	private String descricao;
	private BigDecimal preco;
	private LocalDateTime dataHoraCadastro;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public LocalDateTime getDataHoraCadastro() {
		return dataHoraCadastro;
	}

	public void setDataHoraCadastro(LocalDateTime dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}

	public static ProdutoDto from(Produto produto) {
		if (produto == null)
			throw new IllegalArgumentException("Produto não pode ser nulo");

		ProdutoDto dto = new ProdutoDto();
		dto.setId(produto.getId());
		dto.setDescricao(produto.getDescricao());
		dto.setPreco(produto.getPreco());
		dto.setDataHoraCadastro(produto.getDataHoraCadastro());

		return dto;
	}

	public static List<ProdutoDto> from(List<Produto> produtos) {
		if (produtos == null)
			throw new IllegalArgumentException("Produtos não pode ser nulo");

		return produtos.stream().map(ProdutoDto::from).collect(Collectors.toList());
	}
}
