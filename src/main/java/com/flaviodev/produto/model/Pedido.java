package com.flaviodev.produto.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.flaviodev.produto.model.base.Entidade;
import com.flaviodev.produto.persistence.UUIDGenerator;

@Entity
@Table
public class Pedido extends Entidade<String> {

	private static final long serialVersionUID = -7419565912993585274L;

	@Id
	@GeneratedValue(generator = UUIDGenerator.NAME)
	@GenericGenerator(name = UUIDGenerator.NAME, strategy = UUIDGenerator.PACKAGE_PATH)
	@Column(length = 32)
	private String id;

	@Column(nullable = false)
	private LocalDate data;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "pedido_cliente_fkey"))
	private Cliente cliente;

	@OneToMany(mappedBy = "pedido", fetch = FetchType.LAZY, orphanRemoval = true)
	private List<ItemPedido> itens;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemPedido> getItens() {
		if (itens == null)
			itens = new ArrayList<>();

		return itens;
	}

	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}

	public BigDecimal getValorTotal() {
		return new BigDecimal(getItens().stream().mapToDouble(item -> item.getPrecoTotal().doubleValue()).sum());
	}

	public void adicionaItemAoPedido(ItemPedido item) {
		item.setPedido(this);
		getItens().add(item);
	}

	public void retiraItemDoPedido(ItemPedido item) {
		item.setPedido(null);
		getItens().remove(item);
	}
}
