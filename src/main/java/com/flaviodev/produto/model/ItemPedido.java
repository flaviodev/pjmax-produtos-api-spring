package com.flaviodev.produto.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.flaviodev.produto.model.base.Entidade;
import com.flaviodev.produto.persistence.UUIDGenerator;

@Entity
@Table
public class ItemPedido extends Entidade<String> {

	private static final long serialVersionUID = 1266936087972991477L;

	@Id
	@GeneratedValue(generator = UUIDGenerator.NAME)
	@GenericGenerator(name = UUIDGenerator.NAME, strategy = UUIDGenerator.PACKAGE_PATH)
	@Column(length = 32)
	private String id;

	@Column(nullable = false)
	private BigDecimal precoUnitario;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "itempedido_pedido_fkey"))
	private Pedido pedido;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "itempedido_produto_fkey"))
	private Produto produto;

	@Column(nullable = false)
	private int quantidade;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getPrecoUnitario() {
		return precoUnitario;
	}

	public void setPrecoUnitario(BigDecimal precoUnitario) {
		this.precoUnitario = precoUnitario;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getPrecoTotal() {
		return getPrecoUnitario().multiply(new BigDecimal(getQuantidade()));
	}

}
