package com.flaviodev.produto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import com.flaviodev.produto.model.base.Entidade;
import com.flaviodev.produto.persistence.UUIDGenerator;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "cliente_razao_social_unique", columnNames = "razaoSocial") })
public class Cliente extends Entidade<String> {

	private static final long serialVersionUID = 3682907445895830535L;

	@Id
	@GeneratedValue(generator = UUIDGenerator.NAME)
	@GenericGenerator(name = UUIDGenerator.NAME, strategy = UUIDGenerator.PACKAGE_PATH)
	@Column(length = 32)
	private String id;

	@Column(length = 200, nullable = false)
	private String email;

	@Column(length = 200, nullable = false)
	private String razaoSocial;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
}
