package com.flaviodev.produto.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.flaviodev.produto.ContextoAplicacao;
import com.flaviodev.produto.dto.ProdutoDto;
import com.flaviodev.produto.model.base.Entidade;
import com.flaviodev.produto.persistence.UUIDGenerator;
import com.flaviodev.produto.repository.ProdutoRepository;

@Entity
public class Produto extends Entidade<String> {

	private static final long serialVersionUID = -1121655138970663551L;

	@Id
	@GeneratedValue(generator = UUIDGenerator.NAME)
	@GenericGenerator(name = UUIDGenerator.NAME, strategy = UUIDGenerator.PACKAGE_PATH)
	@Column(length = 32)
	private String id;

	@Column(length = 200, nullable = false)
	private String descricao;

	@Column(nullable = false)
	private BigDecimal preco;

	@Column(nullable = false)
	private LocalDateTime dataHoraCadastro;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public LocalDateTime getDataHoraCadastro() {
		return dataHoraCadastro;
	}

	public void setDataHoraCadastro(LocalDateTime dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}

	private static ProdutoRepository getRepository() {
		return ContextoAplicacao.getRepository(ProdutoRepository.class);
	}

	public static Produto from(ProdutoDto produtoDto) {
		if (produtoDto == null)
			throw new IllegalArgumentException("ProdutoDto não pode ser nulo");

		Produto produto = new Produto();
		produto.setId(produtoDto.getId());
		produto.setDescricao(produtoDto.getDescricao());
		produto.setPreco(produtoDto.getPreco());

		return produto;
	}

	public static Produto getPeloId(String id) {
		Optional<Produto> optional = getRepository().findById(id);

		return optional.isPresent() ? optional.get() : null;
	}

	public static List<Produto> getTodos() {
		List<Produto> produtos = getRepository().findAllByOrderByDescricao();

		if (produtos == null || produtos.isEmpty())
			return Collections.emptyList();

		return produtos;
	}

	private static void validaProduto(ProdutoDto produtoDto) {
		if (produtoDto == null)
			return;

		if (produtoDto.getDescricao() == null || produtoDto.getDescricao().isEmpty())
			throw new IllegalArgumentException("descrição não pode ser nula");

		if (produtoDto.getDescricao().length() > 200)
			throw new IllegalArgumentException("descrição não pode possuir mais de 200 caracteres");

		if (produtoDto.getPreco() == null)
			throw new IllegalArgumentException("preço não pode ser nulo");

		if (produtoDto.getPreco().compareTo(BigDecimal.ZERO) <= 0)
			throw new IllegalArgumentException("preço deve ser maior que zero");
	}

	public static Produto inclui(ProdutoDto produtoDto) {
		validaProduto(produtoDto);
		Produto produto = from(produtoDto);
		produto.setId(null);
		produto.setDataHoraCadastro(LocalDateTime.now());

		return getRepository().save(produto);
	}

	public Produto altera(ProdutoDto produtoDto) {
		validaProduto(produtoDto);
		setDescricao(produtoDto.getDescricao());
		setPreco(produtoDto.getPreco());

		return getRepository().save(this);
	}

	public void exclui() {
		getRepository().delete(this);
	}
}
