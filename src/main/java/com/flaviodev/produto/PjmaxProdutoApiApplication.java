package com.flaviodev.produto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PjmaxProdutoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PjmaxProdutoApiApplication.class, args);
	}
}
