package com.flaviodev.produto.controller;

import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flaviodev.produto.dto.ProdutoDto;
import com.flaviodev.produto.model.Produto;
import com.flaviodev.produto.repository.ProdutoRepository;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

	@Autowired
	private ProdutoRepository produtoRepository;

	@GetMapping("/{id}")
	public ResponseEntity<ProdutoDto> getProduto(@PathVariable("id") String id) {
		Produto produto = Produto.getPeloId(id);

		if (produto == null)
			return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);

		return ResponseEntity.ok(ProdutoDto.from(produto));
	}

	@GetMapping("/descricao/{descricao}")
	public ResponseEntity<List<ProdutoDto>> getProdutosPelaDescricao(@PathVariable("descricao") String descricao) {

		List<Produto> produtos = produtoRepository.findAllByDescricaoStartsWithOrderByDescricao(descricao);

		if (produtos == null || produtos.isEmpty())
			return new ResponseEntity<>(Collections.emptyList(),HttpStatus.NO_CONTENT);

		return ResponseEntity.ok(ProdutoDto.from(produtos));
	}

	@GetMapping
	public ResponseEntity<List<ProdutoDto>> getProdutos() {
		List<Produto> produtos = Produto.getTodos();

		if (produtos == null || produtos.isEmpty())
			return new ResponseEntity<>(Collections.emptyList(),HttpStatus.NO_CONTENT);

		return ResponseEntity.ok(ProdutoDto.from(produtos));
	}

	@GetMapping("/ultimoCadastrado")
	public ResponseEntity<ProdutoDto> getUltimoProdutoCadastrado() {

		Produto produto = produtoRepository.findFirstByOrderByDataHoraCadastroDesc();

		if (produto == null)
			return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);

		return ResponseEntity.ok(ProdutoDto.from(produto));
	}

	@Transactional
	@PostMapping
	public ResponseEntity<ProdutoDto> incluiProduto(@RequestBody ProdutoDto produtoDto) {
		return new ResponseEntity<>(ProdutoDto.from(Produto.inclui(produtoDto)), HttpStatus.CREATED);
	}

	@Transactional
	@PutMapping("/{id}")
	public ResponseEntity<ProdutoDto> alteraProduto(@PathVariable("id") String id, @RequestBody ProdutoDto produtoDto) {
		Produto produto = Produto.getPeloId(id);

		if (produto == null)
			return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);

		produto.altera(produtoDto);

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@Transactional
	@DeleteMapping("/{id}")
	public ResponseEntity<ProdutoDto> excluiProduto(@PathVariable("id") String id) {
		Produto produto = Produto.getPeloId(id);

		if (produto == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		produto.exclui();

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
