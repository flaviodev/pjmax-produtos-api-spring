package com.flaviodev.produto;

import java.io.Serializable;

import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;

import com.flaviodev.produto.model.base.Entidade;

public class ContextoAplicacao implements Serializable {

	private static final long serialVersionUID = -3954490013272185190L;

	private ContextoAplicacao() {

	}

	public static <R extends JpaRepository<E, I>, E extends Entidade<I>, I extends Serializable> R getRepository(
			Class<R> classeRepositorio) {
		ApplicationContext context = ApplicationContextProvider.getContext();

		return context.getBean(classeRepositorio);
	}
}
