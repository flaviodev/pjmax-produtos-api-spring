(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./cadastrar-produto/cadastrar-produto.module": [
		"./src/app/cadastrar-produto/cadastrar-produto.module.ts"
	],
	"./listar-produto/listar-produto.module": [
		"./src/app/listar-produto/listar-produto.module.ts",
		"listar-produto-listar-produto-module"
	],
	"./visualizar-produto/visualizar-produto.module": [
		"./src/app/visualizar-produto/visualizar-produto.module.ts"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-menu></app-menu>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'produto-ui';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.config.ts":
/*!*******************************!*\
  !*** ./src/app/app.config.ts ***!
  \*******************************/
/*! exports provided: MascaraValorMonetarioBrasil */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MascaraValorMonetarioBrasil", function() { return MascaraValorMonetarioBrasil; });
var MascaraValorMonetarioBrasil = {
    align: "right",
    allowNegative: true,
    decimal: ",",
    precision: 2,
    prefix: "R$ ",
    suffix: "",
    thousands: "."
};


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_locales_pt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/locales/pt */ "./node_modules/@angular/common/locales/pt.js");
/* harmony import */ var _angular_common_locales_pt__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_pt__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var ng2_currency_mask_src_currency_mask_config__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-currency-mask/src/currency-mask.config */ "./node_modules/ng2-currency-mask/src/currency-mask.config.js");
/* harmony import */ var ng2_currency_mask_src_currency_mask_config__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_currency_mask_src_currency_mask_config__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _app_config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.config */ "./src/app/app.config.ts");
/* harmony import */ var _produto_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./produto.service */ "./src/app/produto.service.ts");
/* harmony import */ var _menu_menu_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./menu/menu.module */ "./src/app/menu/menu.module.ts");
/* harmony import */ var _cadastrar_produto_cadastrar_produto_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./cadastrar-produto/cadastrar-produto.module */ "./src/app/cadastrar-produto/cadastrar-produto.module.ts");
/* harmony import */ var _visualizar_produto_visualizar_produto_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./visualizar-produto/visualizar-produto.module */ "./src/app/visualizar-produto/visualizar-produto.module.ts");
/* harmony import */ var _produto_resolve__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./produto.resolve */ "./src/app/produto.resolve.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["registerLocaleData"])(_angular_common_locales_pt__WEBPACK_IMPORTED_MODULE_2___default.a, 'pt-BR');
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_7__["routing"],
                _menu_menu_module__WEBPACK_IMPORTED_MODULE_11__["MenuModule"],
                _cadastrar_produto_cadastrar_produto_module__WEBPACK_IMPORTED_MODULE_12__["CadastrarProdutoModule"],
                _visualizar_produto_visualizar_produto_module__WEBPACK_IMPORTED_MODULE_13__["VisualizarProdutoModule"]
            ],
            providers: [
                {
                    provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"],
                    useValue: "pt-BR"
                },
                {
                    provide: ng2_currency_mask_src_currency_mask_config__WEBPACK_IMPORTED_MODULE_8__["CURRENCY_MASK_CONFIG"],
                    useValue: _app_config__WEBPACK_IMPORTED_MODULE_9__["MascaraValorMonetarioBrasil"]
                },
                _produto_service__WEBPACK_IMPORTED_MODULE_10__["ProdutoService"],
                _produto_resolve__WEBPACK_IMPORTED_MODULE_14__["GetProdutosResolver"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routing", function() { return routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");

var ROUTES = [
    { path: 'listarproduto', loadChildren: './listar-produto/listar-produto.module#ListarProdutoModule' },
    { path: '', redirectTo: 'listarproduto', pathMatch: 'full' },
    { path: 'cadastrarproduto', loadChildren: './cadastrar-produto/cadastrar-produto.module#CadastrarProdutoModule' },
    { path: 'alterarproduto/:id', loadChildren: './cadastrar-produto/cadastrar-produto.module#CadastrarProdutoModule' },
    { path: 'ultimoproduto', loadChildren: './visualizar-produto/visualizar-produto.module#VisualizarProdutoModule' },
    { path: 'visualizarproduto/:id', loadChildren: './visualizar-produto/visualizar-produto.module#VisualizarProdutoModule' }
];
var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(ROUTES, { useHash: true }); //, enableTracing: true });


/***/ }),

/***/ "./src/app/cadastrar-produto/cadastrar-produto.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/cadastrar-produto/cadastrar-produto.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-container {\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n}\n   \n.form-right-align {\n    text-align: right;\n}\n   \ninput.form-right-align {\n    -moz-appearance: textfield;\n}\n   \nmat-form-field {\n    width: 100%;\n    margin-bottom: 10px;\n}\n   \nbutton {\n    margin: 2px;\n}\n\n"

/***/ }),

/***/ "./src/app/cadastrar-produto/cadastrar-produto.component.html":
/*!********************************************************************!*\
  !*** ./src/app/cadastrar-produto/cadastrar-produto.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\n    <mat-card-title>{{titulo}}</mat-card-title>\n    <mat-card-content class=\"form-container\">\n      <form [formGroup]=\"formGrupoProduto\">\n        <mat-form-field hintLabel=\"Máximo 200 Caracteres\">\n          <input matInput #inputDescricao placeholder=\"Descrição\" maxlength=\"200\" \n            [(ngModel)]=\"produto.descricao\" formControlName=\"descricao\" required>\n          <mat-hint align=\"end\">{{inputDescricao.value?.length || 0}}/200</mat-hint>\n          <mat-error *ngIf=\"descricao.invalid\">{{getMessageErroDesricao()}}</mat-error>\n        </mat-form-field>\n        <mat-form-field>\n          <input currencyMask matInput placeholder=\"Preço\" type=\"text\" class=\"form-right-align\" \n            [(ngModel)]=\"produto.preco\" formControlName=\"preco\" required>\n          <mat-error *ngIf=\"preco.invalid\">{{getMessageErroPreco()}}</mat-error>\n        </mat-form-field>\n\n        <button mat-raised-button [disabled]=\"!formGrupoProduto.valid\" color=\"primary\" (click)=\"salvar()\">Salvar</button>\n        <button mat-raised-button [routerLink]=\"['/listarproduto']\">Cancelar</button>\n      </form>\n    </mat-card-content>\n  </mat-card>"

/***/ }),

/***/ "./src/app/cadastrar-produto/cadastrar-produto.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/cadastrar-produto/cadastrar-produto.component.ts ***!
  \******************************************************************/
/*! exports provided: CadastrarProdutoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarProdutoComponent", function() { return CadastrarProdutoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _produto_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../produto.service */ "./src/app/produto.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CadastrarProdutoComponent = /** @class */ (function () {
    function CadastrarProdutoComponent(servicoProduto, route, router, formBuilder, snackBar) {
        this.servicoProduto = servicoProduto;
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.titulo = "";
        this.isInclusao = true;
        this.produto = {};
        this.descricao = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        this.preco = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(0.01)]);
        this.formGrupoProduto = this.formBuilder.group({
            descricao: this.descricao,
            preco: this.preco
        });
    }
    CadastrarProdutoComponent.prototype.getMessageErroDesricao = function () {
        return this.descricao.hasError('required') ? 'A descrição do produto é obrigatória' : '';
    };
    CadastrarProdutoComponent.prototype.getMessageErroPreco = function () {
        return this.preco.hasError('required') ? 'Informe o preço do produto' :
            this.preco.hasError('min') ? 'O preço deve ser maior que zero' : '';
    };
    CadastrarProdutoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            if (params['id']) {
                _this.titulo = "Alterar Produto";
                _this.servicoProduto.getProdutoPeloId(params['id'])
                    .subscribe(function (_produto) { return _this.produto = _produto; });
                _this.isInclusao = false;
            }
            else {
                _this.titulo = "Cadastrar Produto";
                _this.isInclusao = true;
            }
        });
    };
    CadastrarProdutoComponent.prototype.salvar = function () {
        var _this = this;
        if (this.isInclusao) {
            this.servicoProduto.incluirProduto(this.produto)
                .subscribe(function (response) { return _this.mostrarMensagemDeSucesso('Produto cadastrado com sucesso!'); });
        }
        else {
            console.log(this.produto.preco);
            this.servicoProduto.alterarProduto(this.produto)
                .subscribe(function (response) { return _this.mostrarMensagemDeSucesso('Produto alterado com sucesso!'); });
        }
    };
    CadastrarProdutoComponent.prototype.mostrarMensagemDeSucesso = function (mensagem) {
        this.snackBar.open(mensagem, '', {
            duration: 3000
        });
        this.router.navigate(['/listarproduto']);
    };
    CadastrarProdutoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cadastrar-produto',
            template: __webpack_require__(/*! ./cadastrar-produto.component.html */ "./src/app/cadastrar-produto/cadastrar-produto.component.html"),
            styles: [__webpack_require__(/*! ./cadastrar-produto.component.css */ "./src/app/cadastrar-produto/cadastrar-produto.component.css")]
        }),
        __metadata("design:paramtypes", [_produto_service__WEBPACK_IMPORTED_MODULE_4__["ProdutoService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], CadastrarProdutoComponent);
    return CadastrarProdutoComponent;
}());



/***/ }),

/***/ "./src/app/cadastrar-produto/cadastrar-produto.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/cadastrar-produto/cadastrar-produto.module.ts ***!
  \***************************************************************/
/*! exports provided: CadastrarProdutoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarProdutoModule", function() { return CadastrarProdutoModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ng2_currency_mask__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-currency-mask */ "./node_modules/ng2-currency-mask/index.js");
/* harmony import */ var ng2_currency_mask__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_currency_mask__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _cadastrar_produto_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cadastrar-produto.component */ "./src/app/cadastrar-produto/cadastrar-produto.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    { path: '', component: _cadastrar_produto_component__WEBPACK_IMPORTED_MODULE_6__["CadastrarProdutoComponent"] }
];
var CadastrarProdutoModule = /** @class */ (function () {
    function CadastrarProdutoModule() {
    }
    CadastrarProdutoModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes),
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                ng2_currency_mask__WEBPACK_IMPORTED_MODULE_5__["CurrencyMaskModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_cadastrar_produto_component__WEBPACK_IMPORTED_MODULE_6__["CadastrarProdutoComponent"]],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], CadastrarProdutoModule);
    return CadastrarProdutoModule;
}());



/***/ }),

/***/ "./src/app/menu/menu.component.css":
/*!*****************************************!*\
  !*** ./src/app/menu/menu.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/menu/menu.component.html":
/*!******************************************!*\
  !*** ./src/app/menu/menu.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<mat-toolbar color=\"primary\">\n  <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n      <mat-icon>menu</mat-icon>\n  </button>\n  <span>Produtos</span>\n</mat-toolbar>\n\n<mat-menu #menu=\"matMenu\">\n  <button mat-menu-item routerLink=\"/listarproduto\">Listar Produtos</button>\n  <button mat-menu-item routerLink=\"/cadastrarproduto\">Cadastrar Produto</button>\n  <button mat-menu-item routerLink=\"/ultimoproduto\">Último Produto Cadastrado</button>\n</mat-menu>"

/***/ }),

/***/ "./src/app/menu/menu.component.ts":
/*!****************************************!*\
  !*** ./src/app/menu/menu.component.ts ***!
  \****************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
    }
    MenuComponent.prototype.ngOnInit = function () {
    };
    MenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.css */ "./src/app/menu/menu.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MenuComponent);
    return MenuComponent;
}());



/***/ }),

/***/ "./src/app/menu/menu.module.ts":
/*!*************************************!*\
  !*** ./src/app/menu/menu.module.ts ***!
  \*************************************/
/*! exports provided: MenuModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuModule", function() { return MenuModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _menu_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menu.component */ "./src/app/menu/menu.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var MenuModule = /** @class */ (function () {
    function MenuModule() {
    }
    MenuModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]
            ],
            declarations: [_menu_component__WEBPACK_IMPORTED_MODULE_3__["MenuComponent"]],
            exports: [_menu_component__WEBPACK_IMPORTED_MODULE_3__["MenuComponent"]]
        })
    ], MenuModule);
    return MenuModule;
}());



/***/ }),

/***/ "./src/app/produto.resolve.ts":
/*!************************************!*\
  !*** ./src/app/produto.resolve.ts ***!
  \************************************/
/*! exports provided: GetProdutosResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetProdutosResolver", function() { return GetProdutosResolver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _produto_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./produto.service */ "./src/app/produto.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GetProdutosResolver = /** @class */ (function () {
    function GetProdutosResolver(produtoService) {
        this.produtoService = produtoService;
    }
    GetProdutosResolver.prototype.resolve = function () {
        console.log("Resolving ..... get produtos");
        return this.produtoService.getProdutos();
    };
    GetProdutosResolver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_produto_service__WEBPACK_IMPORTED_MODULE_1__["ProdutoService"]])
    ], GetProdutosResolver);
    return GetProdutosResolver;
}());



/***/ }),

/***/ "./src/app/produto.service.ts":
/*!************************************!*\
  !*** ./src/app/produto.service.ts ***!
  \************************************/
/*! exports provided: ProdutoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProdutoService", function() { return ProdutoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProdutoService = /** @class */ (function () {
    function ProdutoService(http) {
        this.http = http;
    }
    ProdutoService.prototype.getProdutos = function () {
        return this.http.get("/produto");
    };
    ProdutoService.prototype.getProdutoPeloId = function (idDoProduto) {
        return this.http.get("/produto/" + idDoProduto);
    };
    ProdutoService.prototype.getUltimoProdutoCadastrado = function () {
        return this.http.get("/produto/ultimoCadastrado");
    };
    ProdutoService.prototype.incluirProduto = function (produto) {
        return this.http.post('/produto', produto);
    };
    ProdutoService.prototype.alterarProduto = function (produto) {
        return this.http.put('/produto/' + produto.id, produto);
    };
    ProdutoService.prototype.excluirProduto = function (idDoProduto) {
        return this.http.delete("/produto/" + idDoProduto);
    };
    ProdutoService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ProdutoService);
    return ProdutoService;
}());



/***/ }),

/***/ "./src/app/visualizar-produto/visualizar-produto.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/visualizar-produto/visualizar-produto.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".label {\n    font-weight: bold;\n}"

/***/ }),

/***/ "./src/app/visualizar-produto/visualizar-produto.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/visualizar-produto/visualizar-produto.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\n  <mat-card-title>{{titulo}}</mat-card-title>\n  <p><span class=\"label\">Descrição: </span> {{produto.descricao}}</p>\n  <p><span class=\"label\">Preço: </span> {{produto.preco | currency:'R$'}}</p>\n  <p><span class=\"label\">Data de Cadastro: </span> {{produto.dataHoraCadastro | date:'dd/MM/yyyy HH:mm'}}</p>\n  <button mat-raised-button [routerLink]=\"['/listarproduto']\">Voltar</button>\n</mat-card>"

/***/ }),

/***/ "./src/app/visualizar-produto/visualizar-produto.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/visualizar-produto/visualizar-produto.component.ts ***!
  \********************************************************************/
/*! exports provided: VisualizarProdutoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisualizarProdutoComponent", function() { return VisualizarProdutoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _produto_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../produto.service */ "./src/app/produto.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VisualizarProdutoComponent = /** @class */ (function () {
    function VisualizarProdutoComponent(servicoProduto, route, router) {
        this.servicoProduto = servicoProduto;
        this.route = route;
        this.router = router;
        this.titulo = "";
        this.produto = {};
    }
    VisualizarProdutoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            if (params['id']) {
                _this.titulo = "Dados do Produto";
                _this.servicoProduto.getProdutoPeloId(params['id'])
                    .subscribe(function (_produto) { return _this.produto = _produto; });
            }
            else {
                _this.titulo = "Dados do Último Produto Cadastrado";
                _this.servicoProduto.getUltimoProdutoCadastrado()
                    .subscribe(function (_produto) { return _this.produto = _produto; });
            }
        });
    };
    VisualizarProdutoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-visualizar-produto',
            template: __webpack_require__(/*! ./visualizar-produto.component.html */ "./src/app/visualizar-produto/visualizar-produto.component.html"),
            styles: [__webpack_require__(/*! ./visualizar-produto.component.css */ "./src/app/visualizar-produto/visualizar-produto.component.css")]
        }),
        __metadata("design:paramtypes", [_produto_service__WEBPACK_IMPORTED_MODULE_1__["ProdutoService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], VisualizarProdutoComponent);
    return VisualizarProdutoComponent;
}());



/***/ }),

/***/ "./src/app/visualizar-produto/visualizar-produto.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/visualizar-produto/visualizar-produto.module.ts ***!
  \*****************************************************************/
/*! exports provided: VisualizarProdutoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisualizarProdutoModule", function() { return VisualizarProdutoModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _visualizar_produto_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./visualizar-produto.component */ "./src/app/visualizar-produto/visualizar-produto.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: '', component: _visualizar_produto_component__WEBPACK_IMPORTED_MODULE_2__["VisualizarProdutoComponent"] }
];
var VisualizarProdutoModule = /** @class */ (function () {
    function VisualizarProdutoModule() {
    }
    VisualizarProdutoModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"]
            ],
            declarations: [_visualizar_produto_component__WEBPACK_IMPORTED_MODULE_2__["VisualizarProdutoComponent"]],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
        })
    ], VisualizarProdutoModule);
    return VisualizarProdutoModule;
}());



/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");



Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /usr/src/produto-ui/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map