(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["listar-produto-listar-produto-module"],{

/***/ "./src/app/listar-produto/dialog-confirma-exclusao.component.html":
/*!************************************************************************!*\
  !*** ./src/app/listar-produto/dialog-confirma-exclusao.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Confirmação</h1>\n<div mat-dialog-content>\n  <p>Confirma a exclusão de {{data.produto.descricao}}?</p>\n</div>\n<div mat-dialog-actions>\n  <button mat-button color=\"warn\" [mat-dialog-close]=\"data.produto\">Confirma</button>\n  <button mat-button (click)=\"onCancelaClick()\" cdkFocusInitial>Cancela</button>\n</div>"

/***/ }),

/***/ "./src/app/listar-produto/dialog-confirma-exclusao.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/listar-produto/dialog-confirma-exclusao.component.ts ***!
  \**********************************************************************/
/*! exports provided: DialogConfirmaExclusao */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogConfirmaExclusao", function() { return DialogConfirmaExclusao; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var DialogConfirmaExclusao = /** @class */ (function () {
    function DialogConfirmaExclusao(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DialogConfirmaExclusao.prototype.onCancelaClick = function () {
        this.dialogRef.close();
    };
    DialogConfirmaExclusao = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'dialog-confirma-exclusao',
            template: __webpack_require__(/*! ./dialog-confirma-exclusao.component.html */ "./src/app/listar-produto/dialog-confirma-exclusao.component.html"),
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], DialogConfirmaExclusao);
    return DialogConfirmaExclusao;
}());



/***/ }),

/***/ "./src/app/listar-produto/listar-produto.component.css":
/*!*************************************************************!*\
  !*** ./src/app/listar-produto/listar-produto.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%;\n}\n  \nth.mat-sort-header-sorted {\n  color: black;\n}\n  \n.coluna-descricao {\n  width: 60%;\n}\n  \n.coluna-acoes {\n  width: 50px;\n}\n"

/***/ }),

/***/ "./src/app/listar-produto/listar-produto.component.html":
/*!**************************************************************!*\
  !*** ./src/app/listar-produto/listar-produto.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<mat-card>\n  <mat-card-title>Lista de Produtos</mat-card-title>\n  <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8\">\n    <ng-container matColumnDef=\"descricao\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header> Descrição </th>\n      <td mat-cell class=\"coluna-descricao\" *matCellDef=\"let produto\"> {{produto.descricao}} </td>\n    </ng-container>\n    \n    <ng-container matColumnDef=\"preco\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header> Preço </th>\n      <td mat-cell *matCellDef=\"let produto\"> {{ produto.preco | currency:'R$' }} </td>\n    </ng-container>\n      \n    <ng-container matColumnDef=\"dataHoraCadastro\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header> Data Cadastrado  </th>\n      <td mat-cell *matCellDef=\"let produto\"> {{ produto.dataHoraCadastro | date:'dd/MM/yy HH:mm' }} </td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"btVisualizar\">\n      <th mat-header-cell *matHeaderCellDef></th>\n      <td mat-cell class=\"coluna-acoes\" *matCellDef=\"let produto\"> \n        <button mat-icon-button [routerLink]=\"['/visualizarproduto',produto.id]\" matTooltip=\"Visualizar dados do produto\">\n          <mat-icon>visibility</mat-icon>\n        </button>        \n      </td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"btAlterar\">\n        <th mat-header-cell *matHeaderCellDef></th>\n        <td mat-cell class=\"coluna-acoes\" *matCellDef=\"let produto\"> \n          <button mat-icon-button [routerLink]=\"['/alterarproduto',produto.id]\" matTooltip=\"Editar cadastro do produto\">\n            <mat-icon>edit</mat-icon>\n          </button>\n        </td>\n    </ng-container>\n  \n    <ng-container matColumnDef=\"btExcluir\">\n        <th mat-header-cell *matHeaderCellDef></th>\n        <td mat-cell class=\"coluna-acoes\" *matCellDef=\"let produto\"> \n          <button mat-icon-button (click)=\"confirmaExclusao(produto)\" matTooltip=\"Excluir cadastro do produto\">\n            <mat-icon>delete</mat-icon>\n          </button>\n        </td>\n    </ng-container>\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n  </table>\n</mat-card>\n"

/***/ }),

/***/ "./src/app/listar-produto/listar-produto.component.ts":
/*!************************************************************!*\
  !*** ./src/app/listar-produto/listar-produto.component.ts ***!
  \************************************************************/
/*! exports provided: ListarProdutoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListarProdutoComponent", function() { return ListarProdutoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _produto_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../produto.service */ "./src/app/produto.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dialog_confirma_exclusao_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialog-confirma-exclusao.component */ "./src/app/listar-produto/dialog-confirma-exclusao.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ListarProdutoComponent = /** @class */ (function () {
    function ListarProdutoComponent(servicoProduto, router, dialog, snackBar) {
        this.servicoProduto = servicoProduto;
        this.router = router;
        this.dialog = dialog;
        this.snackBar = snackBar;
        this.displayedColumns = ['descricao', 'preco', 'dataHoraCadastro', 'btVisualizar', 'btAlterar', 'btExcluir'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"]();
    }
    ListarProdutoComponent.prototype.ngOnInit = function () {
        this.dataSource.sort = this.sort;
        this.carregaProdutos();
    };
    ListarProdutoComponent.prototype.carregaProdutos = function () {
        var _this = this;
        this.servicoProduto.getProdutos().subscribe(function (data) { return _this.dataSource.data = data; });
    };
    ListarProdutoComponent.prototype.confirmaExclusao = function (produto) {
        var _this = this;
        var dialogRef = this.dialog.open(_dialog_confirma_exclusao_component__WEBPACK_IMPORTED_MODULE_4__["DialogConfirmaExclusao"], {
            width: '350px',
            data: { produto: produto }
        });
        dialogRef.afterClosed().subscribe(function (produto) {
            console.log(produto);
            if (produto) {
                _this.servicoProduto.excluirProduto(produto.id)
                    .subscribe(function (response) {
                    _this.mostrarMensagemDeSucesso('Produto excluído com sucesso!');
                    _this.dataSource.data = _this.dataSource.data.filter(function (p) { return p.id != produto.id; });
                });
            }
        });
    };
    ListarProdutoComponent.prototype.mostrarMensagemDeSucesso = function (mensagem) {
        this.snackBar.open(mensagem, '', {
            duration: 3000
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], ListarProdutoComponent.prototype, "sort", void 0);
    ListarProdutoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-listar-produto',
            template: __webpack_require__(/*! ./listar-produto.component.html */ "./src/app/listar-produto/listar-produto.component.html"),
            styles: [__webpack_require__(/*! ./listar-produto.component.css */ "./src/app/listar-produto/listar-produto.component.css")]
        }),
        __metadata("design:paramtypes", [_produto_service__WEBPACK_IMPORTED_MODULE_2__["ProdutoService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"]])
    ], ListarProdutoComponent);
    return ListarProdutoComponent;
}());



/***/ }),

/***/ "./src/app/listar-produto/listar-produto.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/listar-produto/listar-produto.module.ts ***!
  \*********************************************************/
/*! exports provided: ListarProdutoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListarProdutoModule", function() { return ListarProdutoModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _produto_resolve__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../produto.resolve */ "./src/app/produto.resolve.ts");
/* harmony import */ var _listar_produto_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./listar-produto.component */ "./src/app/listar-produto/listar-produto.component.ts");
/* harmony import */ var _dialog_confirma_exclusao_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dialog-confirma-exclusao.component */ "./src/app/listar-produto/dialog-confirma-exclusao.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    { path: '', component: _listar_produto_component__WEBPACK_IMPORTED_MODULE_7__["ListarProdutoComponent"],
        resolve: { items: _produto_resolve__WEBPACK_IMPORTED_MODULE_6__["GetProdutosResolver"] } }
];
var ListarProdutoModule = /** @class */ (function () {
    function ListarProdutoModule() {
    }
    ListarProdutoModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild(routes),
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSortModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBarModule"]
            ],
            declarations: [
                _listar_produto_component__WEBPACK_IMPORTED_MODULE_7__["ListarProdutoComponent"],
                _dialog_confirma_exclusao_component__WEBPACK_IMPORTED_MODULE_8__["DialogConfirmaExclusao"]
            ],
            entryComponents: [_dialog_confirma_exclusao_component__WEBPACK_IMPORTED_MODULE_8__["DialogConfirmaExclusao"]],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"]]
        })
    ], ListarProdutoModule);
    return ListarProdutoModule;
}());



/***/ })

}]);
//# sourceMappingURL=listar-produto-listar-produto-module.js.map